import React, { Component } from 'react';
import { TextInput } from "./lib";

class App extends Component {
  render() {
    return (
      <div style={{ width: 640, margin: "15px auto" }}>
        <h1>Dodigo Component Library</h1>
        <TextInput label="Email Address" placeholder="name@example.com" />
      </div>
    );
  }
}

export default App;
